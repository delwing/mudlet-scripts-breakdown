scriptsLoaded = scriptsLoaded or false

function loadScripts(force)
    if not force and scriptsLoaded then
        return
    end

    cecho("\n<CadetBlue>(skrypty)<tomato>: Laduje pliki skryptow\n")

    mudletModules = {}
    require("scriptsList")

    for k, v in pairs(mudletModules) do
        package.loaded[v] = nil
        require(v)
    end
end

loadScripts(false)
