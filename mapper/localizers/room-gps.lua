function temp_triggers_mapper_localizers_room_gps_ruiny_rinde_za_krata()
    amap:print_log("GPS po nazwie/wyjsciach lokacji: Ruiny Rinde, za krata (4 poziom)", true)
    amap:set_position(19137, true)
end

function temp_triggers_mapper_localizers_room_gps_ruiny_za_nekromanta()
    amap:print_log("GPS po nazwie/wyjsciach lokacji: Za woda z nekromanty w Ruinach Rinde", true)
    amap:set_position(1925, true)
end

function temp_triggers_mapper_localizers_room_gps_karczma_gnijaca_meduza_ard()
    amap:print_log("GPS po nazwie/wyjsciach lokacji: Karczma 'Gnijaca Meduza'", true)
    amap:set_position(10422, true)
end

function temp_triggers_mapper_localizers_room_gps_gora_karczmy_gnijaca_meduza_ard()
    amap:print_log("GPS po nazwie/wyjsciach lokacji: Gora Karczmy 'Gnijaca Meduza'", true)
    amap:set_position(10423, true)
end

function temp_triggers_mapper_localizers_room_gps_mario()
    amap:print_log("GPS po nazwie/wyjsciach lokacji: <orange>Mario", true)
    amap:set_position(5471, true)
end

