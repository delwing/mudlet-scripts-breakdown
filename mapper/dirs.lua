function temp_aliases_mapper_dirs_sneaky_w()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("west", false)
    end
end

function temp_aliases_mapper_dirs_sneaky_e()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("east", false)
    end
end

function temp_aliases_mapper_dirs_sneaky_n()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("north", false)
    end
end

function temp_aliases_mapper_dirs_sneaky_s()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("south", false)
    end
end

function temp_aliases_mapper_dirs_sneaky_sw()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("southwest", false)
    end
end

function temp_aliases_mapper_dirs_sneaky_se()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("southeast", false)
    end
end

function temp_aliases_mapper_dirs_sneaky_nw()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("northwest", false)
    end
end

function temp_aliases_mapper_dirs_sneaky_ne()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("northeast", false)
    end
end

function temp_aliases_mapper_dirs_przemknij_u()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("up", false)
    end
end

function temp_aliases_mapper_dirs_przemknij_d()
    send(matches[2], false)

    if amap.mode ~= "off" then
        amap["went_sneaky"] = true
        amap:follow("down", false)
    end
end

