scripts = scripts or { ver = "3.16d" }
scripts.event_handlers = scripts.event_handlers or {}

function temp_aliases_skrypty_fake()
    local s = matches[2]

    s = string.gsub(s, "%$", "\n")
    feedTriggers("\n" .. s .. "\n")
    echo("\n")
end

function temp_aliases_skrypty_lua_code()
    local f, e = loadstring("return " .. matches[2])
    if not f then
        f, e = assert(loadstring(matches[2]))
    end

    local r = f()
    if r ~= nil then display(r) end
end

function temp_timer_skrypty_hidden_timer()
    if scripts.ui.info_hidden_value == 14 then
        scripts.ui.info_hidden_value = "ok"
        scripts.ui.states_window_nav_states["hidden_state"] = "ok"
        disableTimer("hidden_timer")
    else
        scripts.ui.info_hidden_value = scripts.ui.info_hidden_value + 1
        scripts.ui.states_window_nav_states["hidden_state"] = scripts.ui.states_window_nav_states["hidden_state"] + 1
    end
    raiseEvent("hidden_state", scripts.ui.info_hidden_value)
end

function temp_timer_skrypty_cover_timer()
    if scripts.ui.cover_wait_time == 1 then
        scripts.ui.cover_wait_time = "ok"
        scripts.ui.states_window_nav_states["guard_state"] = "ok"
        disableTimer("cover_timer")
    else
        scripts.ui.cover_wait_time = scripts.ui.cover_wait_time - 1
        scripts.ui.states_window_nav_states["guard_state"] = scripts.ui.states_window_nav_states["guard_state"] - 1
    end
    raiseEvent("guard_state", scripts.ui.cover_wait_time)
end

function temp_timer_skrypty_order_timer()
    if scripts.ui.order_wait_time == 1 then
        scripts.ui.order_wait_time = "ok"
        scripts.ui.states_window_nav_states["order_state"] = "ok"
        disableTimer("order_timer")
    else
        scripts.ui.order_wait_time = scripts.ui.order_wait_time - 1
        scripts.ui.states_window_nav_states["order_state"] = scripts.ui.states_window_nav_states["order_state"] - 1
    end
    raiseEvent("order_state", scripts.ui.order_wait_time)
end

