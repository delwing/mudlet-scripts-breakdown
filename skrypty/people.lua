scripts["people"] = scripts["people"] or { db = {}, mail = {}, enemies = {} }

scripts.people.db = db:create("people", {
    people = {
        name = "",
        short = "",
        title = "",
        updated = "",
        note = "",
        enemy = 0,
        guild = 0,
        room_id = -1,
        changed = db:Timestamp("CURRENT_TIMESTAMP"),
        _index = { "name", "short" }
    }
})

scripts.people["showing_names"] = true
scripts.people["updating_names"] = false

scripts.people["name_color"] = "yellow"
scripts.people["guild_color"] = "dark_orange"

scripts.people["people_triggers"] = scripts.people["people_triggers"] or {}
scripts.people["people_triggers_objects"] = scripts.people["people_triggers_objects"] or {}

scripts.people.enemy_guilds = {}
scripts.people.enemy_people = {}
scripts.people.enemy_table = {}
scripts.people.enemy_suffix = {}

scripts.people.keep_binds_unchanged = false
scripts.people.show_binds_setting = 1

scripts.people.colored_guilds = {}
scripts.people.colored_people = {}
scripts.people.color_table = {}
scripts.people.color_suffix = {}
scripts.people.color_items = {}

scripts.people.trigger_guilds = {
    "CKN", "ES", "SC", "KS", "KM", "OS",
    "OHM", "SGW", "PE", "WKS", "LE", "KG",
    "KGKS", "MC", "OK", "RA", "GL", "ZT",
    "ZS", "ZH", "GP"
}
--scripts.people.trigger_guilds = {}
scripts.people.trigger_items = {}
scripts.people.trigger_suffix = {}

