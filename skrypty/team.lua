ateam = ateam or {
    all_numbering = false,
    can_msg = true,
    objs = {},
    prev_enemy = nil,
    people_on_location = {},
    team = {},
    team_names = {},
    my_id = nil,
    next_team_id = "A",
    to_support = nil,
    cover_command = nil,
    is_enemy_on_location = nil,
    attack_mode = 3,
    clicked_second_defense = false,
    clicked_second_attack = false,
    special_follow_bind_mode = 1,
    release_guards = false,
    attack_commands = { "zabij" },
    support_command = "wesprzyj",
    sneaky_attack = 0,
    sneaky_attack_cond = "ok",
    paralyzed_names = {},
    paralyzed_name_to_safety_timer = {},
    options = {
        team_numbering_mode = "mode1",
        team_mate_stun_bg_color = "goldenrod",
        team_mate_stun_fg_color = "black",
        enemy_stun_bg_color = "orchid",
        enemy_stun_fg_color = "black",
    }
}

states = { [6] = "<green>#######", [5] = "<green>-######", [4] = "<yellow>--#####", [3] = "<yellow>---####", [2] = "<red>----###", [1] = "<red>-----##", [0] = "<red>------#" }
states_no_color = { [6] = "#######", [5] = "-######", [4] = "--#####", [3] = "---####", [2] = "----###", [1] = "-----##", [0] = "------#" }

disableTrigger("Zabil-counter")

ateam["footer_info_attack_mode_to_text"] = { "A", "AW", "AWR" }

function temp_triggers_skrypty_team_start_ateam()
    tempTimer(4, [[ ateam:start_ateam() ]])
    tempTimer(5, function() scripts.ui:setup_talk_window() end)
end

function temp_triggers_skrypty_team_restart_ateam()
    tempTimer(3, [[ ateam:restart_ateam() ]])
    tempTimer(3.5, function() scripts.ui:setup_talk_window() end)
end

function temp_aliases_skrypty_team_przelam()
    if scripts.character.state.fatigue > -1 and scripts.character.state.fatigue <= scripts.character.break_fatigue_level then
        sendAll("przestan kryc sie za zaslona", "przelam obrone celu ataku", false)
    end
end

function temp_aliases_skrypty_team_rozkaz_ataku()
    if gmcp.objects.nums and ateam.current_attack_target and ateam.current_attack_target_relative and table.contains(gmcp.objects.nums, ateam.current_attack_target) then
        ateam:ra_func(ateam.current_attack_target_relative)
    end
end

function temp_aliases_skrypty_team_rozkaz_zaslony()
    if gmcp.objects.nums and ateam.current_defense_target and ateam.current_defense_target_relative and table.contains(gmcp.objects.nums, ateam.current_defense_target) then
        ateam:rz_func(ateam.current_defense_target_relative)
    end
end

function temp_aliases_skrypty_team_rozkaz_blokady()
    if gmcp.objects.nums and ateam.current_attack_target and ateam.current_attack_target_relative and table.contains(gmcp.objects.nums, ateam.current_attack_target) then
        ateam:rb_func(ateam.current_attack_target_relative)
    end
end

