scripts.inv["available_types"] = {
    ["money"] = true,
    ["gems"] = true,
    ["food"] = true,
    ["other"] = true,
}

scripts.inv["bag_name"] = {
    ["plecak"] = 1,
    ["torba"] = 2,
    ["worek"] = 3,
    ["sakiewka"] = 4,
    ["mieszek"] = 5,
    ["sakwa"] = 6,
    ["wor"] = 7,
    ["szkatulka"] = 8,
}

scripts.inv["bag_in_dopelniacz"] = {
    ["plecak"] = "plecaka",
    ["torba"] = "torby",
    ["worek"] = "worka",
    ["sakiewka"] = "sakiewki",
    ["mieszek"] = "mieszka",
    ["sakwa"] = "sakwy",
    ["wor"] = "wora",
    ["szkatulka"] = "szkatulki",
}

scripts.inv["bag_in_biernik"] = {
    ["plecak"] = "plecak",
    ["torba"] = "torbe",
    ["worek"] = "worek",
    ["sakiewka"] = "sakiewke",
    ["mieszek"] = "mieszek",
    ["sakwa"] = "sakwe",
    ["wor"] = "wor",
    ["szkatulka"] = "szkatulke",
}

scripts.inv["money_bag_1"] = "plecak"
scripts.inv["gems_bag_1"] = "plecak"
scripts.inv["food_bag_1"] = "plecak"
scripts.inv["other_bag_1"] = "plecak"


