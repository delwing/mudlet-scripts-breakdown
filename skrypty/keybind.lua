scripts["keybind"] = scripts["keybind"] or {}
scripts.keybind.ids = scripts.keybind.ids or {}

scripts.keybind.printable_items = {
    ["Control"] = "CTRL",
    ["Alt"] = "Alt",
    ["Shift"] = "Shift",
    ["Meta"] = "Meta",
    ["Keypad"] = "Keypad",
    ["GroupSwitch"] = "GroupSwitch",
    ["Equal"] = "=",
    ["Plus"] = "+",
    ["Minus"] = "-",
    ["Asterisk"] = "*",
    ["Ampersand"] = "&",
    ["AsciiCircum"] = "^",
    ["Percent"] = "*",
    ["Dollar"] = "*",
    ["NumberSign"] = "*",
    ["At"] = "*",
    ["Exclam"] = "*",
    ["AsciiTilde"] = "~",
    ["BracketLeft"] = "[",
    ["BracketRight"] = "]",
    ["BraceLeft"] = "{",
    ["BraceRight"] = "}",
    ["ParenLeft"] = "(",
    ["ParenRight"] = ")",
    ["QuoteLeft"] = "`",
    ["QuoteDbl"] = "\"",
    ["Apostrophe"] = "'",
    ["Less"] = "<",
    ["Greater"] = ">",
    ["Slash"] = "/",
    ["Backslash"] = "\\",
    ["Underscore"] = "_",
    ["Comma"] = ",",
    ["Period"] = ".",
    ["Question"] = "_",
    ["Colon"] = ":",
    ["Semicolon"] = ";",
    ["Bar"] = "|",
}

scripts.keybind["configuration"] = {
    ["fight_support"] = {
        ["modifier"] = { "Control" },
        ["key"] = "W",
        ["description"] = "Wsparcie",
        ["callback"] = "callback_fight_support",
        ["active"] = true
    },
    ["attack_target"] = {
        ["modifier"] = { "Control" },
        ["key"] = "1",
        ["description"] = "Atakowanie celu ataku",
        ["callback"] = "callback_attack_target",
        ["active"] = true
    },
    ["critical_hp"] = {
        ["modifier"] = { "Control", "Alt" },
        ["key"] = "Equal",
        ["description"] = "Uzycie kondycji w niskim stanie hp",
        ["callback"] = "callback_critical_hp",
        ["active"] = true
    },
    ["functional_key"] = {
        ["modifier"] = {},
        ["key"] = "BracketRight",
        ["description"] = "Funkcjonalny bind",
        ["callback"] = "callback_functional_bind",
        ["active"] = true
    },
    ["attack_bind_obj"] = {},
    ["attack_bind_objs"] = {
        ["modifier"] = {},
        ["key"] = { "F1" },
        ["description"] = "Atakowanie osob z bindow",
        ["callback"] = "callback_bind_attack_objs",
        ["active"] = true
    },
    ["break_attack_target"] = {
        ["modifier"] = {},
        ["key"] = "F2",
        ["description"] = "Przelamywanie i atakowanie celu ataku",
        ["callback"] = "callback_break_defense",
        ["active"] = true
    },
    ["block_attack_target"] = {
        ["modifier"] = {},
        ["key"] = "F3",
        ["description"] = "Blokowanie celu ataku",
        ["callback"] = "callback_block_attack_obj",
        ["active"] = true
    },
    ["collect_from_body"] = {
        ["modifier"] = { "Control" },
        ["key"] = "3",
        ["description"] = "Zbieranie z cial",
        ["callback"] = "callback_collect_from_body",
        ["active"] = true
    },
    ["filling_lamp"] = {
        ["modifier"] = { "Control" },
        ["key"] = "4",
        ["description"] = "Dopelnianie lampy",
        ["callback"] = "callback_filling_lamp",
        ["active"] = true
    },
    ["empty_bottle"] = {
        ["modifier"] = {},
        ["key"] = "Backslash",
        ["description"] = "Odlozenie pustej butelki",
        ["callback"] = "callback_empty_bottle",
        ["active"] = true
    },
    ["enter_ship"] = {
        ["modifier"] = {},
        ["key"] = "BracketLeft",
        ["description"] = "Wsiadanie na statki i dylizanse",
        ["callback"] = "callback_enter_ship",
        ["active"] = true
    },
    ["temp1"] = {
        ["modifier"] = { "Control" },
        ["key"] = "Minus",
        ["description"] = "Tymczasowy keybind (1)",
        ["callback"] = "callback_temp1",
        ["active"] = true
    },
    ["temp2"] = {
        ["modifier"] = { "Control" },
        ["key"] = "Equal",
        ["description"] = "Tymczasowy keybind (2)",
        ["callback"] = "callback_temp2",
        ["active"] = true
    },
    ["temp3"] = {
        ["modifier"] = { "Alt" },
        ["key"] = "Minus",
        ["description"] = "Tymczasowy keybind (3)",
        ["callback"] = "callback_temp3",
        ["active"] = true
    },
    ["temp4"] = {
        ["modifier"] = { "Alt" },
        ["key"] = "Equal",
        ["description"] = "Tymczasowy keybind (4)",
        ["callback"] = "callback_temp4",
        ["active"] = true
    },
    ["temp5"] = {
        ["modifier"] = {},
        ["key"] = "",
        ["description"] = "Tymczasowy keybind (5)",
        ["callback"] = "callback_temp5",
        ["active"] = false
    },
    ["temp6"] = {
        ["modifier"] = {},
        ["key"] = "",
        ["description"] = "Tymczasowy keybind (6)",
        ["callback"] = "callback_temp6",
        ["active"] = false
    },
    ["temp7"] = {
        ["modifier"] = {},
        ["key"] = "",
        ["description"] = "Tymczasowy keybind (7)",
        ["callback"] = "callback_temp7",
        ["active"] = false
    },
    ["temp8"] = {
        ["modifier"] = {},
        ["key"] = "",
        ["description"] = "Tymczasowy keybind (8)",
        ["callback"] = "callback_temp8",
        ["active"] = false
    },
    ["temp9"] = {
        ["modifier"] = {},
        ["key"] = "",
        ["description"] = "Tymczasowy keybind (9)",
        ["callback"] = "callback_temp9",
        ["active"] = false
    },
    ["temp10"] = {
        ["modifier"] = {},
        ["key"] = "",
        ["description"] = "Tymczasowy keybind (10)",
        ["callback"] = "callback_temp10",
        ["active"] = false
    },
    ["opening_gate"] = {
        ["modifier"] = { "Control" },
        ["key"] = "2",
        ["description"] = "Otwieranie bram",
        ["callback"] = "callback_opening_gate",
        ["active"] = true
    },
    ["drinking"] = {
        ["modifier"] = { "Control" },
        ["key"] = "N",
        ["description"] = "Picie ze zrodel wody",
        ["callback"] = "callback_drinking",
        ["active"] = true
    },
    ["special_exit"] = {
        ["modifier"] = { "Control" },
        ["key"] = "P",
        ["description"] = "Bindy do przejsc specjalnych",
        ["callback"] = "callback_special_exit",
        ["active"] = true
    },
    ["walk_mode"] = {
        ["modifier"] = {},
        ["key"] = "QuoteLeft",
        ["description"] = "Tryby chodzenia",
        ["callback"] = "callback_walk_mode",
        ["active"] = true
    },
}

