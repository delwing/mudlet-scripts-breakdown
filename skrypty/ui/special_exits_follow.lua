function temp_triggers_skrypty_ui_special_exits_follow_furtka_otwiera()
    if matches[2] then
        scripts.utils.bind_functional_team_follow(matches[2], "furtka", 15)
    else
        scripts.utils.bind_functional_team_follow(nil, "furtka", 15)
    end
end

function temp_triggers_skrypty_ui_special_exits_follow_wejdz_w_krzaki()
    if amap and amap.curr and amap.curr.id == 7372 then
        scripts.utils.bind_functional_team_follow(matches[2], "przecisnij sie przez krzaki", 15)
        return
    end

    scripts.utils.bind_functional_team_follow(matches[2], "wejdz w krzaki", 15)
end

function temp_triggers_skrypty_ui_special_exits_follow_wejdz_na_gore()
    if amap and amap.curr and amap.curr.id == 17944 then
        return
    end
    scripts.utils.bind_functional_team_follow(matches[2], "wejdz na gore", 15)
end

function temp_triggers_skrypty_ui_special_exits_follow_wejdz_po_drabinie()
    if amap and amap.curr and amap.curr.id == 17944 then
        scripts.utils.bind_functional_team_follow(matches[2], "wespnij sie na gore", 15)
        return
    end
    scripts.utils.bind_functional_team_follow(matches[2], "opusc bronie;wejdz po drabinie;dobadz wszystkich broni", 15)
end

function temp_triggers_skrypty_ui_special_exits_follow_wcisnij_plaskorzezbe()

    scripts.utils.bind_functional_team_follow(matches[2], "wcisnij plaskorzezbe", 15)
end

